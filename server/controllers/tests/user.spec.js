import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { User } from '../../models';
import Users, { returnSignInResult } from '../user';
import { success } from '../../utils/responseHandler';

jest.mock('../../models', () => ({
  User: { create: jest.fn(), scope: jest.fn(), findOne: jest.fn(), findAll: jest.fn() },
}));

jest.mock('../../utils/responseHandler', () => ({
  success: jest.fn(),
  error: jest.fn(),
}));

jest.mock('jsonwebtoken', () => ({
  verify: jest.fn(),
  sign: jest.fn(),
}));

jest.mock('bcrypt', () => ({
  hashSync: jest.fn(),
  compare: jest.fn(),
}));

describe('user controller', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    User.scope.mockReturnValue(User);
  });

  it('should return one User', async () => {
    const req = { header: () => getHeader(), params: { id: 1, login: 'teste' } },
      res = {
        status: () => res,
        send: () => res,
      };

    User.findOne.mockResolvedValueOnce(() => ({ data: {} }));

    await Users.getOne(req, res);
    expect(User.findOne).toBeCalledWith({ where: { id: req.params.id, login: req.params.login } });
  });

  it('should return all Users', async () => {
    const req = { header: () => getHeader(), params: { id: 1, login: 'teste' } },
      res = {
        status: () => res,
        send: () => res,
      };

    const serviceResponse = { data: [] };

    User.findAll.mockResolvedValueOnce(serviceResponse);

    await Users.getAll(req, res);
    expect(User.findAll).toBeCalled();
    expect(success).toBeCalledWith(res, 200, serviceResponse);
  });

  it('should sign in', async () => {
    const req = { header: () => getHeader(), body: { login: 'teste', password: '12345' } },
      res = {
        status: () => res,
        send: jest.fn(),
      };
    const serviceResponse = { data: [] };

    User.findOne.mockResolvedValueOnce({ login: 'teste', password: '12345' });
    jwt.sign.mockReturnValueOnce('token');

    await Users.signIn(req, res);

    expect(User.findOne).toBeCalledWith({ where: { login: req.body.login } });
    expect(bcrypt.compare).toBeCalled();
  });

  it('should return user when signed in', async () => {
    const req = { header: () => getHeader(), body: { login: 'teste', password: '12345' } },
      res = {
        status: () => res,
        send: jest.fn(),
      };

    const token = 'token';
    const user = { login: 'teste', password: '12345' };
    const userWithoutPassword = { login: user.login };

    jwt.sign.mockReturnValueOnce(token);

    await Users.returnSignInResult(user, res);
    expect(res.send).toBeCalledWith({ user: userWithoutPassword, token });
  });

  it('should return user when signed up', async () => {
    const req = { header: () => getHeader(), body: { login: 'teste', password: '12345' } },
      res = {
        status: () => res,
        send: jest.fn(),
      };

    const token = 'token';
    const user = { login: 'teste', password: '12345' };
    const userData = { ...user, id: 1 };
    const hashPassword = 'hashPassword';

    jwt.sign.mockReturnValueOnce(token);
    bcrypt.hashSync.mockReturnValueOnce(hashPassword);
    User.create.mockResolvedValueOnce(userData);

    await Users.signUp(req, res);
    expect(success).toBeCalledWith(res, 201, userData);
  });
});

const getHeader = () => {
  return 'Bearer 12345';
};

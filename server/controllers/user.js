import model from '../models';
import bcrypt from 'bcrypt';
import { error, success } from '../utils/responseHandler';
import { ERRO_BUSCAR_USUARIO, ERRO_INSERIR_USUARIO } from '../utils/messages';
import jwt from 'jsonwebtoken';

const { User } = model;
const salt = 15;

class Users {
  static async signUp(req, res) {
    const { login, password } = req.body;

    const encryptedPass = bcrypt.hashSync(password, salt);

    return User.create({
      login,
      password: encryptedPass,
    })
      .then(userData => success(res, 201, userData))
      .catch(() => {
        error(res, 500, ERRO_INSERIR_USUARIO);
      });
  }

  static signIn(req, res) {
    const { login, password } = req.body;

    User.scope('withPassword')
      .findOne({
        where: {
          login,
        },
      })
      .then(user => {
        bcrypt.compare(
          password,
          user && user.password,
          (err, result) => (result && Users.returnSignInResult(user, res)) || error(res, 401, ERRO_BUSCAR_USUARIO)
        );
      })
      .catch(err => {
        console.log(err);
        error(res, 500, ERRO_BUSCAR_USUARIO);
      });
  }

  static getAll(req, res) {
    return User.findAll().then(users => success(res, 200, users));
  }

  static getOne(req, res) {
    const { id, login } = req.params;
    return User.findOne({
      where: {
        id,
        login,
      },
    });
  }

  static returnSignInResult(user, res) {
    const token = jwt.sign(
      {
        _id: user.id,
        _login: user.login,
      },
      process.env.JWT_KEY
    );
    const withoutPassword = user;
    withoutPassword.password = undefined;
    return res.status(200).send({
      user: withoutPassword,
      token,
    });
  }
}

/* export const returnSignInResult = (user, res) => {
  const token = jwt.sign(
    {
      _id: user.id,
      _login: user.login,
    },
    process.env.JWT_KEY
  );
  const withoutPassword = user;
  withoutPassword.password = undefined;
  return res.status(200).send({
    user: withoutPassword,
    token,
  });
}; */

export default Users;

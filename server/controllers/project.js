import model from '../models';
import { error, success } from '../utils/responseHandler';
import { ERRO_INSERIR_PROJETO } from '../utils/messages';

const { Project } = model;

class Projects {

  static insert(req, res) {
    const { name } = req.body;

    return Project.create({ name })
      .then(projectData => success(res, 200, projectData))
      .catch(() => {
        error(res, 500, ERRO_INSERIR_PROJETO);
      });
  }

  static getAll(req, res) {
    return Project.findAll().then(projects => success(res, 200, projects));
  }

  static getOne(req, res) {
    const { id } = req.params;
    return Project.findById(id).then(project => project && success(res, 200, project) || success(res, 204));
  }
}

export default Projects;
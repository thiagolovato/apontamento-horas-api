import model from '../models';
import { error, success } from '../utils/responseHandler';
import { ERRO_VINCULAR_USUARIO_PROJETO } from '../utils/messages';

const { UserProjectPermission, Project } = model;

class UserProjectPermissions {
  static insert(req, res) {
    const { userId, projectId } = req.body;

    return UserProjectPermission.create({ userId, projectId })
      .then(uppData => success(res, 200, uppData))
      .catch(() => {
        error(res, 500, ERRO_VINCULAR_USUARIO_PROJETO);
      });
  }

  static getProjectsByUserIdOrProjectId(req, res) {
    const { userId, projectId } = req.params;

    const where = {};
    userId && (where.userId = userId);
    projectId && (where.projectId = projectId);

    return UserProjectPermission.findAll({
      where,
      include: [{ model: Project }],
    }).then(projects => {
      return res.status(200).send(projects);
    });
  }

  static getAll(req, res) {
    return UserProjectPermission.findAll().then(projects => success(res, 200, projects));
  }
}

export default UserProjectPermissions;

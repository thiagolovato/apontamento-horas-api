import model from '../models';
import { error, success } from '../utils/responseHandler';
import { ERRO_VINCULAR_USUARIO_PROJETO, ERRO_BUSCAR_ALOCACOES } from '../utils/messages';

const { AllocationUserProject, User } = model;

class AllocationUserProjects {
  static insert(req, res) {
    const { userId, projectId, allocationDate, allocationTime } = req.body;

    return AllocationUserProject.create({ userId, projectId, allocationDate, allocationTime })
      .then(uppData => success(res, 200, uppData))
      .catch(erro => {
        console.log(erro);
        error(res, 500, ERRO_VINCULAR_USUARIO_PROJETO);
      });
  }

  static async getAllocationByProject(req, res) {
    const { userId, projectId } = req.params;
    const where = {};

    const user = await User.findOne({
      where: {
        id: userId,
      },
    }).then(user => {
      return user;
    });

    userId && !user.isAdmin && (where.userId = userId);
    projectId && !user.isAdmin && (where.projectId = projectId);

    return AllocationUserProject.findAll({
      where,
      include: [{ all: true }],
    })
      .then(projects => {
        return res.status(200).send(projects);
      })
      .catch(() => {
        error(res, 500, ERRO_BUSCAR_ALOCACOES);
      });
  }

  static getAll(req, res) {
    return AllocationUserProject.findAll({
      include: [{ all: true }],
    }).then(projects => success(res, 200, projects));
  }
}

export default AllocationUserProjects;

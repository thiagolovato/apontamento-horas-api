import User from '../controllers/user';
import Project from '../controllers/project';
import UserProjectPermissions from '../controllers/userProjectPermission';
import AllocationUserProjects from '../controllers/allocationUserProject';
import { auth } from '../middleware/auth';

export default app => {
  app.get('/users', auth, User.getAll);
  app.get('/users/:id', auth, User.getOne);
  app.post('/users', User.signUp);
  app.post('/login', User.signIn);

  app.post('/projects', auth, Project.insert);
  app.get('/projects/', auth, Project.getAll);
  app.get('/projects/:id', auth, Project.getOne);

  app.get('/user-project/:userId/:projectId', auth, UserProjectPermissions.getProjectsByUserIdOrProjectId);
  app.get('/user-project/:userId', auth, UserProjectPermissions.getProjectsByUserIdOrProjectId);
  app.post('/user-project', auth, UserProjectPermissions.insert);

  app.get('/user-allocation/:userId/:projectId', auth, AllocationUserProjects.getAllocationByProject);
  app.get('/user-allocation/:userId', auth, AllocationUserProjects.getAllocationByProject);
  app.get('/user-allocation/', auth, AllocationUserProjects.getAll);
  app.post('/user-allocation', auth, AllocationUserProjects.insert);
};

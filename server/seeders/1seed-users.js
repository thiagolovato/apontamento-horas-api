module.exports = {
  up: async (queryInterface, Sequelize) => {
    const qt = await queryInterface.sequelize.query(`SELECT count(1) from "Users"`);
    qt === 0 &&
      queryInterface.bulkInsert(
        'Users',
        [
          {
            id: 1,
            login: 'administrador@zallpy.com',
            password: '$2b$15$PUnyYoyAIJlqpZ2gFTSM0.rnsv3CRsZC9khWr1RSgJ8x9ONGiEaGy',
            isAdmin: true,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          {
            id: 2,
            login: 'programador1@zallpy.com',
            password: '$2b$15$PUnyYoyAIJlqpZ2gFTSM0.rnsv3CRsZC9khWr1RSgJ8x9ONGiEaGy',
            isAdmin: false,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          {
            id: 3,
            login: 'programador2@zallpy.com',
            password: '$2b$15$PUnyYoyAIJlqpZ2gFTSM0.rnsv3CRsZC9khWr1RSgJ8x9ONGiEaGy',
            isAdmin: false,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
        ],
        {}
      );
  },

  down: queryInterface => queryInterface.bulkDelete('Users', null, {}),
};

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      'UserProjectPermissions',
      [
        {
          userId: 1,
          projectId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 1,
          projectId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 2,
          projectId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 3,
          projectId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 3,
          projectId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    ),

  down: queryInterface => queryInterface.bulkDelete('UserProjectPermissions', null, {}),
};

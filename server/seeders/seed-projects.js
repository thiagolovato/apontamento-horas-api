module.exports = {
  up: async (queryInterface, Sequelize) => {
    const qt = await queryInterface.sequelize.query(`SELECT count(1) from "Projects"`);
    qt === 0 &&
      queryInterface.bulkInsert(
        'Projects',
        [
          {
            id: 1,
            name: 'Projeto Cliente A',
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          {
            id: 2,
            name: 'Projeto Cliente B',
            createdAt: new Date(),
            updatedAt: new Date(),
          },
        ],
        {}
      );
  },

  down: queryInterface => queryInterface.bulkDelete('Projects', null, {}),
};

export const success = (res, status, data) => {
  return res.status(status)
    .send({
      success: status >= 200 && status < 300,
      data,
    });
};

export const error = (res, status, message) => {
  return res.status(status)
    .send({
      success: false,
      message: message,
    });
};
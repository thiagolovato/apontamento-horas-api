export const ERRO_INSERIR_USUARIO = 'Erro ao inserir usuário. Favor revisar os dados informados.';
export const ERRO_BUSCAR_USUARIO = 'Usuário ou senha inválidos. Favor revisar os dados informados.';
export const ERRO_USUARIO_DESLOGADO = 'Por favor, refaça o login.';

export const ERRO_INSERIR_PROJETO = 'Erro ao inserir projeto. Favor revisar os dados informados.';
export const ERRO_VINCULAR_USUARIO_PROJETO = 'Erro ao vincular usuário ao projeto. Favor revisar os dados informados.';

export const ERRO_BUSCAR_ALOCACOES = 'Erro ao buscar os dados de alocação.';

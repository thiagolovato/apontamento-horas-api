import jwt from 'jsonwebtoken';
import User from '../controllers/user';
import { ERRO_USUARIO_DESLOGADO } from '../utils/messages';

export const auth = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const data = jwt.verify(token, process.env.JWT_KEY);

    let request = {
      params: {
        id: data._id,
        login: data._login,
      },
    };

    req.params.userId = data._id;
    req.params.login = data._login;

    User.getOne(request, res)
      .then(user => {
        return next();
      })
      .catch(() => {
        returnError(res);
        return;
      });
  } catch (err) {
    returnError(res);
    return;
  }
};

const returnError = res => {
  res.status(401).send({
    error: ERRO_USUARIO_DESLOGADO,
  });
};

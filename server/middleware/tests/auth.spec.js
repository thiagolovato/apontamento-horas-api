import jwt from 'jsonwebtoken';
import { auth } from '../auth';
import User from '../../controllers/user';
import { ERRO_USUARIO_DESLOGADO } from '../../utils/messages';

jest.mock('../../controllers/user', () => ({
  getOne: jest.fn(),
}));

jest.mock('jsonwebtoken', () => ({
  verify: jest.fn(),
}));

describe('auth', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should call user controller to get logged user', async () => {
    const req = { header: () => getHeader(), params: {} },
      res = {
        status: () => res,
        send: () => res,
      },
      next = jest.fn();

    User.getOne.mockResolvedValueOnce(() => ({ data: {} }));

    jwt.verify.mockReturnValueOnce({ _id: 1, _login: 'teste' });

    await auth(req, res, next);
    expect(User.getOne).toBeCalledWith({ params: { id: 1, login: 'teste' } }, res);
    expect(next).toBeCalled();
  });

  it('should throw exception when user not found', async () => {
    const req = { header: () => getHeader(), params: {} };
    const res = {
      status: () => res,
      send: jest.fn(),
    };
    const next = jest.fn();

    User.getOne.mockRejectedValueOnce(() => {});

    const jwtReturn = { _id: 1, _login: 'teste' };
    jwt.verify.mockReturnValueOnce(jwtReturn);

    await auth(req, res, next);
    expect(jwt.verify).toBeCalled();
    expect(User.getOne).toBeCalledWith({ params: { id: 1, login: 'teste' } }, res);
    expect(next).not.toBeCalled();
  });

  it('should throw exception when token verification fail', async () => {
    const req = { header: () => getHeader(), params: {} };
    const res = {
      status: () => res,
      send: jest.fn(),
    };
    const next = jest.fn();

    User.getOne.mockRejectedValueOnce(() => {});

    jwt.verify.mockImplementation(() => {
      throw new Error('JWT is not valid!');
    });

    await auth(req, res, next);
    expect(User.getOne).not.toBeCalled();
    expect(next).not.toBeCalled();
  });
});

const getHeader = () => {
  return 'Bearer 12345';
};

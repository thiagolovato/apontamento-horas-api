module.exports = (sequelize, DataTypes) => {
  const upp = sequelize.define(
    'UserProjectPermission',
    {
      userId: DataTypes.INTEGER,
      projectId: DataTypes.INTEGER,
    },
    {}
  );
  upp.associate = models => {
    upp.belongsTo(models.Project, { foreignKey: 'projectId' });
    upp.belongsTo(models.User, { foreignKey: 'userId' });
  };
  return upp;
};

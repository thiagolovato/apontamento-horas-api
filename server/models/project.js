export default (sequelize, DataTypes) => {
  const project = sequelize.define('Project', {
    name: DataTypes.STRING,
  });

  project.associate = models => {
    project.hasMany(models.AllocationUserProject, { foreignKey: 'projectId' });
  };
  return project;
};

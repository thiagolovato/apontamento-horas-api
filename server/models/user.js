export default (sequelize, DataTypes) => {
  const user = sequelize.define(
    'User',
    {
      login: DataTypes.STRING,
      password: DataTypes.STRING,
      isAdmin: DataTypes.BOOLEAN,
    },
    {
      defaultScope: {
        attributes: { exclude: ['password'] },
      },
      scopes: {
        withPassword: {
          attributes: { include: ['password'] },
        },
      },
    }
  );
  user.associate = models => {
    user.hasMany(models.AllocationUserProject, { foreignKey: 'userId' });
  };
  return user;
};

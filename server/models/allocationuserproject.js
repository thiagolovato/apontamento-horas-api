module.exports = (sequelize, DataTypes) => {
  const aup = sequelize.define('AllocationUserProject', {
    userId: DataTypes.INTEGER,
    projectId: DataTypes.INTEGER,
    allocationDate: DataTypes.DATE,
    allocationTime: DataTypes.STRING,
  });

  aup.associate = models => {
    aup.belongsTo(models.Project, { foreignKey: 'projectId' });
    aup.belongsTo(models.User, { foreignKey: 'userId' });
  };
  return aup;
};

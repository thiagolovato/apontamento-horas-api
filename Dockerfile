FROM node:carbon

# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:

ENV JWT_KEY jwtkeytounlockthesecretsofourpasswords

ADD package.json /tmp/package.json
RUN cd /tmp && npm i --silent && npm install -g sequelize-cli --silent

RUN mkdir -p /app && cp -a /tmp/node_modules /app

# copy current working directory into docker; but it first checks for  
# .dockerignore so build will not be included.

COPY      . /app
WORKDIR   /app

# remove any previous builds and create a new build folder and then
# call our node script deploy

RUN rm -f build
RUN mkdir build
RUN chmod 777 ./build
RUN npm run deploy

RUN npm run test

# Migration
RUN sequelize db:migrate
RUN sequelize db:seed:all

EXPOSE 3001


ENTRYPOINT ["node","./build/index.js"]